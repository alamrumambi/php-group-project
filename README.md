# FINAL PROJECT

## Kelompok 8
<hr>

## Anggota Kelompok
<hr>

- Rendy Noor Chandra
- Augyeris Lioga Seandrio
- Nipa Alam Rumambi

<br>

## Tema Project
<hr>
Ecommerce Shoes Store

<br>
<br>

## ERD
<hr>

![ERD](public/ERD/tugasgrup.jpg)

<br>

## Link Video
<hr>
Link Demo Aplikasi : https://drive.google.com/file/d/1zpAnJI_tJOQLuwGpx9S6FJFySxImlIry/view?usp=sharing
<br>
Link Deploy : http://sanber-shoes.herokuapp.com/