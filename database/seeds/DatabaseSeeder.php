<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(user_seed::class);
        $this->call(items_seed::class);
        $this->call(salaries_seed::class);
        $this->call(customer_seed::class);
    }
}
