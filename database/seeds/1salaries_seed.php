<?php

use Illuminate\Database\Seeder;
use App\Salary;

class salaries_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Salary::insert([
            [
                "salary"=>500,
                "user_id"=>1
            ]
        ]);
    }
}
