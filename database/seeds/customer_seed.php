<?php

use Illuminate\Database\Seeder;
use App\Customer;

class customer_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Customer::insert([
            [
                "name"=>"customer",
                "email"=>"customer@email.com",
                "password"=>bcrypt('customer123'),
                "role"=>'customer'
            ]
        ]);
    }
}
