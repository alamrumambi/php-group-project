<?php

use Illuminate\Database\Seeder;
use App\Item;

class items_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/shoes.json");
        $data = json_decode($json);
        foreach($data as $obj) {
            Item::create(array(
                "name" => $obj->name,
                "price" => $obj->price,
                "stock" => $obj->stock,
                "category" => $obj->category,
                "description" => $obj->description,
                "pict_url" => $obj->pict_url,
                "user_id" => $obj->user_id
            ));
        }
        //
    }
}
