<?php

use Illuminate\Database\Seeder;
use App\User;

class user_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                "name"=>"admin",
                "email"=>"admin@email.com",
                "password"=>bcrypt('admin123'),
                "role"=>'admin'
            ]
        ]);
        //
    }
}
