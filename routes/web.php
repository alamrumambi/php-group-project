<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::middleware('auth:customer')->group(function(){
//     Route::get('/', function () {
//         return view('layouts.user.index');
//     });
//     // Tulis routemu di sini.
// });
// Route::get('/home', 'HomeController@index')->name('index');
// Route::get('customer/login', 'Auth\CustomerAuthController@getLogin')->name('customer.login');
// Route::post('customer/login', 'Auth\CustomerAuthController@postLogin');
// Route::get('/', function () {
//     return view('user.index');
// });

// Route::get('/admin', function(){
//     return view('auth.register');
// });

Route::get('/', 'homeController@index');
Route::get('/show-all/{filter}', 'homeController@findAll');
Route::get('/show-item/{id}', 'homeController@findById');

Route::group(['middleware' => ['auth']], function () {
    //
    Route::get('/admin-page', 'adminController@homeAdmin');
    Route::resource('/admin-page/items', 'ItemController');
    Route::resource('/admin-page/admins', 'adminController');
    Route::get('/admin-page/customers', 'customerController@index');
    Route::get('/admin-page/transactions', 'transactionController@index');
});
// Route::middleware('auth:user')->group(function(){
// });

Route::resource('/customers', 'customerController');
Route::resource('/transactions', 'transactionController');
Route::get('admin-page/login', 'Auth\UserAuthController@getLogin')->name('user.login');
Route::post('admin-page/login', 'Auth\UserAuthController@postLogin');
Route::get('admins/logout', 'Auth\UserAuthController@postLogout');

// Route::get('/shop', function(){
//     return view('layouts.user.shop');
// });


// Route::resource('jenis', 'JenisController');

// Route::resource('sepatu', 'SepatuController');
// =======
// Route::get('/register', function(){
//     return view('layouts.user.register');
// });

// Route::get('/detail', function(){
//     return view('layouts.user.detail');
// });
