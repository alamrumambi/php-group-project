@extends('layouts.master')

@section('judul')
    8 Group Store
@endsection

@include('layouts.partial.navbar')
@section('isi')

<a href="/jenis/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">nama</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($jenis as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            <a href="/jenis/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/jenis/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/jenis/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection