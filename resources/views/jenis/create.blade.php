@extends('layouts.master')

@section('judul')
    8 Group Store
@endsection

@include('layouts.partial.navbar')
@section('isi')
<div>
        <form action="/jenis" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama jenis</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
