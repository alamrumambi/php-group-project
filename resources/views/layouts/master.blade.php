<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('judul')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('layouts.partial.navbar')
    <link rel="apple-touch-icon" href="{{asset('admin/assets/img/apple-icon.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('admin/assets/img/Favicon_1.ico')}}">

    <link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/css/templatemo.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/css/custom.css')}}">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{asset('admin/assets/css/fontawesome.min.css')}}">
<!--
-->
@stack('style')
{{-- @include('layouts.partial.navbar') --}}
</head>
<body>
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">@yield('judul')</h3>
        </div>
        <div class="card-body">
          @yield('isi')
        </div>
        <!-- /.card-body -->
      </div>
    <!-- Start Script -->
    <script src="{{asset('admin/assets/js/jquery-1.11.0.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/templatemo.js')}}"></script>
    <script src="{{asset('admin/assets/js/custom.js')}}"></script>
    <!-- End Script -->

    @stack('script')
    
  @include('layouts.partial.footer')
  </body>




</html>