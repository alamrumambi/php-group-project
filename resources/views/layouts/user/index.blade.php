@extends('layouts.master')

@section('judul')
    8 Group Store
@endsection

@include('layouts.partial.navbar')

    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    @include('layouts.partial.brand')


    <!-- Start Categories of The Month -->
    <section class="container py-5">
        <div class="row text-center pt-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Categories</h1>
                <p>
                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="/shop"><img src="{{asset('admin/assets/img/casual-shoes-1.jpg')}}" class="rounded-circle img-fluid border"></a>
                <h5 class="text-center mt-3 mb-3">Casual</h5>
                <p class="text-center"><a href="/show-all/casual" class="btn btn-success">Go Shop Casual Shoes</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="/shop"><img src="{{asset('admin/assets/img/sport-shoes-1.jpeg')}}" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Sport</h2>
                <p class="text-center"><a href="/show-all/sport" class="btn btn-success">Go Shop Sport Shoes</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="/shop"><img src="{{asset('admin/assets/img/boots-shoes-1.jpg')}}" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Boots</h2>
                <p class="text-center"><a href="/show-all/boots" class="btn btn-success">Go Shop Boots</a></p>
            </div>
        </div>
    </section>
    <!-- End Categories of The Month -->


    <!-- Start Featured Product -->
    <section class="bg-light">
        <div class="container py-5">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto">
                    <h1 class="h1">Featured Product</h1>
                    <p>
                        Reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident.
                    </p>
                </div>
            </div>
            <div class="row">
                @forelse ($items as $item)
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="/show-item/{{ $item->id }}">
                            <img src="{{ strpos($item->pict_url, 'http') !== false ? $item->pict_url : asset('items_images/'.$item->pict_url) }}" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body">
                            <ul class="list-unstyled d-flex justify-content-between">
                                <li class="text-muted text-right">${{ $item->price }}</li>
                            </ul>
                            <a href="/show-item/{{ $item->id }}" class="h2 text-decoration-none text-dark">{{ $item->name }}</a>
                            <p class="card-text">
                                {{ $item->description }}
                            </p>
                        </div>
                    </div>
                </div>    
                @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr> 
                @endforelse
                
                
            </div>
        </div>
    </section>
    <!-- End Featured Product -->
    @include('layouts.partial.footer')

    <!-- Start Script -->
    <script src="{{asset('admin/assets/js/jquery-1.11.0.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/templatemo.js')}}"></script>
    <script src="{{asset('admin/assets/js/custom.js')}}"></script>
    <!-- End Script -->
</body>

</html>