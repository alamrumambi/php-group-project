@extends('adminlte.master');

@section('head-title')
    <h1>Detail Admin</h1>
@endsection

@section('title')
    <h3 class="card-title">Admin</h3>
@endsection

@section('content')
<form action="">
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input disabled type="text" value="{{ $item->name }}" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Email</label>
    <input disabled type="text" value="{{ $item->email }}" name="price" class="form-control" id="exampleInputPassword1">
  </div>
</form>
<a href="/admin-page/admins" class="btn btn-primary">Back</a>
@endsection