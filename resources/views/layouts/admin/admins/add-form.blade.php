@extends('adminlte.master');

@section('head-title')
    <h1>Add New Admin</h1>
@endsection

@section('title')
    <h3 class="card-title">Add Admin</h3>
@endsection

@section('content')
<form action="/admin-page/admins" method="POST">
  @csrf
  @error('name')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  @error('email')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">email</label>
    <input type="email" name="email" class="form-control" id="exampleInputPassword1">
  </div>
  @error('password')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword2" class="form-label">Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword2">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection