@extends('adminlte.master');

@section('head-title')
    <h1>Edit Admin</h1>
@endsection

@section('title')
    <h3 class="card-title">Edit Admin</h3>
@endsection

@section('content')
<form action="/admin-page/admins/{{ $item->id }}" method="POST">
  @method('PUT')
  @csrf
  @error('name')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input value="{{ $item->name }}" type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  @error('email')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">email</label>
    <input value="{{ $item->email }}" type="email" name="email" class="form-control" id="exampleInputPassword1">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection