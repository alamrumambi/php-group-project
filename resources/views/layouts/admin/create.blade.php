@extends('layouts.master')

@section('judul')
    8 Group Store
@endsection

@include('layouts.partial.navbar')

@section('isi')

<div>
    <form action="/berita" method="POST">
        @csrf
        <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>content</label>
            <input type="text" class="form-control" name="content" placeholder="Masukkan content">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection