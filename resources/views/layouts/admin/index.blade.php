@extends('adminlte.master')

@section('head-title')
    <h1>WELCOME</h1>
@endsection

@section('content')
    <h3>Welcome to Admin Page for Content Manajemen System Group 8 Shoes Store</h3>
@endsection

@push('scripts')
<script>
    swal({
      title: "Welcome!",
      text: "Welcome to admin page",
      icon: "success",
    });
</script>
@endpush