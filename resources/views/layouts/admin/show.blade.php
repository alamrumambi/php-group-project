@extends('layouts.master')

@section('judul')
    8 Group Store
@endsection

@include('layouts.partial.navbar')

@section('isi')
<h4>{{$berita->judul}}</h4>
<p>{{$berita->content}}</p>
@endsection