@extends('adminlte.master');

@section('head-title')
    <h1>Edit Item</h1>
@endsection

@section('title')
    <h3 class="card-title">Edit Item</h3>
@endsection

@section('content')
<form action="/admin-page/items/{{ $item->id }}" method="POST" enctype="multipart/form-data">
  @method('PUT')
  @csrf
  @error('name')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" value="{{ $item->name }}" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  @error('price')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Price</label>
    <input type="text" value="{{ $item->price }}" name="price" class="form-control" id="exampleInputPassword1">
  </div>
  @error('stock')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword2" class="form-label">Stock</label>
    <input type="number" value="{{ $item->stock }}" name="stock" class="form-control" id="exampleInputPassword2">
  </div>
  @error('category')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword2" class="form-label">Category</label><br>
    <select class="form-select" name="category" aria-label="Default select example">
        <option selected>Open this select category</option>
        <option {{ $item->category === 'casual' ? 'selected': '' }} value="casual">Casual</option>
        <option {{ $item->category === 'sport' ? 'selected': '' }} value="sport">Sport</option>
        <option {{ $item->category === 'boots' ? 'selected': '' }} value="boots">Boots</option>
      </select>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword2" class="form-label">Description</label>
    <textarea class="form-control" name="description" placeholder="write description here" id="floatingTextarea2" style="height: 100px">{{ $item->description }}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection