@extends('adminlte.master');

@section('head-title')
    <h1>Items</h1>
@endsection

@section('title')
    <h3 class="card-title">Data Items</h3>
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Items</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
    @endif
    @if (session('success-delete'))
    <div class="alert alert-danger">
      {{ session('success-delete') }}
    </div>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>Price</th>
        <th>Stock</th>
        <th>Category</th>
        <th>Description</th>
        <th>Picture</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
        
      @forelse ($items as $key=>$item)
      <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->price }}</td>
        <td>{{ $item->stock }}</td>
        <td>{{ $item->category }}</td>
        <td>{{ $item->description }}</td>
        <td><img src="{{ strpos($item->pict_url, 'http') !== false ? $item->pict_url : asset('items_images/'.$item->pict_url) }}" width="50px" alt=""></td>
        <td>
          <a href="/admin-page/items/{{ $item->id }}" class="btn btn-info">Show</a> &nbsp;
          <a href="/admin-page/items/{{ $item->id }}/edit" class="btn btn-primary">Edit</a> &nbsp;
          <form action="/admin-page/items/{{ $item->id }}" method="POST">
            @csrf
            @method('DELETE')
            <input onclick="return confirm('are you sure to delete?')" type="submit" class="btn btn-danger my-1" value="Delete">
          </form>
        </td>
      </tr>
      @empty
        <tr colspan="3">
          <td>No data</td>
        </tr>  
      @endforelse 

      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush