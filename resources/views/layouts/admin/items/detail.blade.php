@extends('layouts.master')

@section('judul')
    Product Detail
@endsection


@section('isi')

<div class="mb-4 colorlib-product">
    <div class="container">
        <div class="row row-pb-lg product-detail-wrap">
            <div class="col-sm-8">
                <div class="owl-carousel">
                    <div class="item">
                        <div class="product-entry border">
                            <a href="#" class="prod-img">
                                <img src="{{ strpos($item->pict_url, 'http') !== false ? $item->pict_url : asset('items_images/'.$item->pict_url) }}" class="img-fluid" alt="Free html5 bootstrap 4 template">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="product-desc">
                    <h3>{{ $item->name }}</h3>
                    <p class="price">
                        <span>${{ $item->price }}</span> 
                    </p>
                    <p>{{ $item->description }}</p>
                    <div class="size-wrap">
                        <div class="block-26 mb-2">
                            <h4>Size</h4>
                        <select name="" id="">
                            <option value="">34</option>
                            <option value="">35</option>
                            <option value="">36</option>
                        </select>
                    </div>
                    <div class="block-26 mb-4">
                            <h4>Width</h4>
                       <select name="" id="">
                           <option value="">M</option>
                           <option value="">L</option>
                           <option value="">XL</option>
                       </select>
                    </div>
                </div>
            <a class="btn btn-primary" href="/admin-page/items">Back</a>
        </div>
    </div>
</div>


@endsection