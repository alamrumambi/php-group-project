@extends('adminlte.master');

@section('head-title')
    <h1>Customers</h1>
@endsection

@section('title')
    <h3 class="card-title">Customers</h3>
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Customers</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
    @endif
    @if (session('success-delete'))
    <div class="alert alert-danger">
      {{ session('success-delete') }}
    </div>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>email</th>
      </tr>
      </thead>
      <tbody>
        
      @forelse ($items as $key=>$item)
      <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->email }}</td>
      </tr>
      @empty
        <tr colspan="3">
          <td>No data</td>
        </tr>  
      @endforelse 

      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush