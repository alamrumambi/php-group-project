@extends('adminlte.master');

@section('head-title')
    <h1>Transactions</h1>
@endsection

@section('title')
    <h3 class="card-title">Transactions</h3>
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Transactions</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
    @endif
    @if (session('success-delete'))
    <div class="alert alert-danger">
      {{ session('success-delete') }}
    </div>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Customer Id</th>
        <th>Item Id</th>
        <th>Quantity</th>
        <th>Total Price</th>
      </tr>
      </thead>
      <tbody>
        
      @forelse ($items as $key=>$item)
      <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $item->customer_id }}</td>
        <td>{{ $item->item_id }}</td>
        <td>{{ $item->quantity }}</td>
        <td>{{ $item->total_price }}</td>
      </tr>
      @empty
        <tr colspan="3">
          <td>No data</td>
        </tr>  
      @endforelse 

      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush