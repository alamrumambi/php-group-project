@extends('layouts.master')

@section('judul')
    8 Group Store
@endsection

@include('layouts.partial.navbar')

@section('isi')


<div>
    <form action="/berita/{{$berita->id}}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control" name="judul" value="{{$berita->judul}}" placeholder="Masukkan Nama">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Content</label>
            <input type="text" class="form-control" name="content" value="{{$berita->content}}" placeholder="Masukkan umur">
            @error('content')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection