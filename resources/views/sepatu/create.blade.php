@extends('layouts.master')

@section('judul')
    Masukkan data sepatu
@endsection

@include('layouts.partial.navbar')
@section('isi')

<div>
    <form action="/sepatu" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Ukuran</label>
            <input type="text" class="form-control" name="ukuran" placeholder="Masukkan ukuran">
            @error('ukuran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Jenis</label>
            <select class="form-control" name="jenis_id" id="exampleFormControlSelect1">
                <option> -- Pilih Jenis --</option>
                @foreach ($jenis as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
              </select>
          
            @error('jenis_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Poster</label>
            <input type="file" class="form-control-file" name="poster">
          </div>
          @error('poster')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection