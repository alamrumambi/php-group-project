<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $table = 'items';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'price',
        'stock',
        'category',
        'description',
        'pict_url',
        'user_id'
    ];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
