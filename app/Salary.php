<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    //
    protected $table = 'salaries';
    protected $primaryKey = 'id';
    protected $fillable = [
        'salary',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
