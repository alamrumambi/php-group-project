<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items = Item::limit(3)->orderBy('id', 'desc')->get();
        return view('layouts.user.index', ["items" => $items]);
    }

    public function findAll($filter)
    {
        $items = [];
        if ($filter !== 'all') {
            $items = Item::where('category', $filter)->orderBy('id', 'desc')->get();
        } else {   
            $items = Item::orderBy('id', 'desc')->get();;
        }
        // dd($items);
        return view('layouts.user.shop', ["items" => $items, "category" => $filter]);
    }

    public function findById($id) {
        $item = Item::where('id', $id)->first();
        return view('layouts.user.detail', ["item" => $item]);
    }
}
