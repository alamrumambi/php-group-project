<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use File;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderBy('id', 'desc')->get();
        return view('layouts.admin.items.index', ["items" => $items]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.admin.items.add-form');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:items',
            'price' => 'required',
            'stock' => 'required',
            'category' => 'required',
            'description' => 'required'
        ]);

        if ($request->file('pict_url')) {
            $file = $request->file('pict_url');
            $file_name = time()."_".$file->getClientOriginalName();
            $file->move('items_images', $file_name);
        }

        Item::create([
            "name" => $request["name"],
            "price" => $request["price"],
            "stock" => $request["stock"],
            "category" => $request["category"],
            "description" => $request["description"],
            "pict_url" => $file_name,
            "user_id" => 1
        ]);
        return redirect('/admin-page/items')->with('success', 'New item added successfully');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::where('id', $id)->first();
        return view('layouts.admin.items.detail', ["item" => $item]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Item::where('id', $id)->first();
        // dd(strpos($item->pict_url, 'http'));
        return view('layouts.admin.items.edit-form', ["item" => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'category' => 'required',
            'description' => 'required'
        ]);

        $item = Item::find($id);
        $item->name = $request->name;
        $item->price = $request->price;
        $item->stock = $request->stock;
        $item->category = $request->category;
        $item->description = $request->description;
        $item->update();
        return redirect('/admin-page/items')->with('success', "cast with id $id updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item = Item::find($id);
        $item->delete();
        return redirect('/admin-page/items')->with('success-delete', "item with id $id deleted successfully");
    }
}
