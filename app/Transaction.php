<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = 'transactions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'item_id',
        'customer_id',
        'quantity',
        'total_price'
    ];
    public $timestamps = true;

    public function item()
    {
        return $this->belongsToMany('App\Item');
    }

    public function customer()
    {
        return $this->belongsToMany('App\Customer');
    }
}
